from flask import Flask, abort
from gevent.wsgi import WSGIServer
import pygame
import time
import queue
import yaml
import threading 


app = Flask(__name__)
q = queue.Queue()
configured_events = None


def play_sound(sound_file, repeats):
    pygame.mixer.init()
    pygame.mixer.music.load(sound_file)
    for i in range(repeats):
        pygame.mixer.music.play()
        while pygame.mixer.music.get_busy() == True:
            time.sleep(0.2)
            continue


def player():
    while True:
        next_play = q.get()
        play_sound(next_play.get('file'), next_play.get('repeats', 1))
        q.task_done()


@app.route('/ring/<event>', methods=['GET'])
def ring_event(event):
    if event not in configured_events:
        abort(404)
    q.put_nowait(configured_events[event])
    return ''


def load_events():
    global configured_events
    fp = open('config.yml') 
    configured_events = yaml.load(fp)


def main():
    load_events()
    http_server = WSGIServer(('', 3000), app)
    th = threading.Thread(target=player)
    th.start()
    print('Ready to serve...')
    http_server.serve_forever()


if __name__ == '__main__':
    main()

